//
//  ViewController.swift
//  SocialLogin
//
//  Created by INDRESH on 2/21/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
     @IBOutlet weak var uidLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var photoUrlLabel: UILabel!
    
    var handle: Auth?
    var uid: String = "" {
        didSet {
            uidLabel.text = "\(uid)"
        }
    }
    var email: String = "" {
        didSet {
            emailLabel.text = "\(email)"
        }
    }
    var photoURL: String = "" {
        didSet {
            photoUrlLabel.text = "\(photoURL)"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            print(auth)
        print(user)
        
        if let user = user {
          // The user's ID, unique to the Firebase project.
          // Do NOT use this value to authenticate with your backend server,
          // if you have one. Use getTokenWithCompletion:completion: instead.
            self.uid = user.uid
            self.email = user.email ?? "N/A"
            guard let url = user.photoURL else {
                return
            }
            self.photoURL = "\(url)"
          // ...
        }else{
            self.uid = ""
            self.email = ""
            self.photoURL = ""
        }
       }) as? Auth
        
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        guard let han = handle else {return}
        Auth.auth().removeStateDidChangeListener(han)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func signInButtonTap(_ sender: UIButton) {
        guard let email = emailTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authDataResult, error in
            print(error?.localizedDescription as Any)
            print(authDataResult as Any)
            guard let strongSelf = self else { return }
            print(strongSelf )
        }
    }
    
    @IBAction func updateDisplayNameButtonTap(_ sender: UIButton) {
        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
               changeRequest?.displayName = "INDRESH SINGH"
               changeRequest?.commitChanges { (error) in
                for data in Auth.auth().currentUser!.providerData {
                    print(data.displayName as Any)
                    self.photoURL = data.displayName ?? "N/A"
                 }
               }
    }
    
    @IBAction func updateEmailButtonTap(_ sender: UIButton) {
        Auth.auth().currentUser?.updateEmail(to: "indresh1@embin.com", completion: { (error) in
            print(error?.localizedDescription as Any)
        })
    }
    
    @IBAction func sendVerficationLinkButtonTap(_ sender: UIButton) {
        Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
            print(error?.localizedDescription as Any)
        })
    }
    
    @IBAction func signoutButtonTap(_ sender: UIButton) {
        let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
        } catch let signOutError as NSError {
          print ("Error signing out: %@", signOutError)
        }
    }
    
    @IBAction func checkStatusButtonClicked(_ sender: UIButton) {
        if Auth.auth().currentUser != nil {
          // User is signed in.
          // ...
            for data in Auth.auth().currentUser!.providerData {
                print(data)
            }
           
            print("User is signed in.")
            self.photoURL = "User is signed in."
        } else {
          // No user is signed in.
          // ...
            print("No user is signed in.")
            self.photoURL = "No user is signed in."
        }
    }
    @IBAction func deleteUserButtonClicked(_ sender: UIButton) {
        guard let user = Auth.auth().currentUser else {return}
        user.delete { (error) in
            print(error?.localizedDescription as Any)
        }
    }
}

