//
//  GoogleController.swift
//  SocialLogin
//
//  Created by INDRESH on 2/24/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn


class GoogleController: UIViewController {

    @IBOutlet weak var signInButton: GIDSignInButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        Auth.auth().addStateDidChangeListener({ (auth, user) in
             print(auth)
            print(user?.email)
            print(user?.displayName)
            print(user?.photoURL)
         
         
        }) as? Auth
        
    }
    
    @IBAction func googleSignIn(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func signoutButtonTap(_ sender: UIButton) {
          let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
        } catch let signOutError as NSError {
          print ("Error signing out: %@", signOutError)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
