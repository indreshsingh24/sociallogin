//
//  PhoneController.swift
//  SocialLogin
//
//  Created by INDRESH on 2/25/20.
//  Copyright © 2020 INDRESH. All rights reserved.
//

import UIKit
import Firebase

class PhoneController: UIViewController {

    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        Auth.auth().addStateDidChangeListener({ (auth, user) in
                    print(auth)
                   print(user?.email)
                   print(user?.displayName)
                   print(user?.photoURL)
            print(user?.phoneNumber)
                
                
               }) as? Auth
    }
    

    @IBAction func sendSMSButtonClicked(_ sender: UIButton) {
        guard let phoneNumber = phoneNumberTextField.text else {return}
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
          if let error = error {
            print(error.localizedDescription)
            return
          }
          // Sign in using the verificationID and the code sent to the user
          // ...
            print("verificationID: \(verificationID)")
            if let verificationID = verificationID {
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            }
            
        }
    }
    
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        guard let verification = UserDefaults.standard.string(forKey: "authVerificationID") else { return  }
        guard let otp = otpTextField.text else {return}
        
        let credential = PhoneAuthProvider.provider().credential(
        withVerificationID: verification,
        verificationCode: otp)
    
    
    Auth.auth().signIn(with: credential) { (authResult, error) in
      if let error = error {
        // ...
        print("Error Phone Number: \(error.localizedDescription)")
        return
      }
      // User is signed in
      // ...
        print("Auth Result: \(authResult?.additionalUserInfo)")
    }
}
}
